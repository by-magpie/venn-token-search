export type pureCount = number;
export type tokenLayer = Map<number, [pureCount, Map<number, unknown>]>;
export type tokenQuantityMap = Map<string, number>;
export type tokenQuantityMapCompressed = Map<number, number>;
export type tokenFileMap = Map<string, Map<number, number>>;
export type tokenFileMapCompressed = Map<number, Map<number, number>>;
export type tokenIdMap = Map<number, string>;
export type fileIdMap = Map<number, string>;