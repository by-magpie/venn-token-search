/**
 * This should generate all the cache files needed.
 */
import Spinner from 'https://deno.land/x/cli_spinners@v0.0.2/mod.ts';
import { walk } from "https://deno.land/std@0.181.0/fs/mod.ts";
import { clearCache, getCachePerms, getDataPerms } from './test-premissions.ts';
import { safeEncode } from './safe-msg-pack.ts';
import { generateTokenCache } from './cache-generator/generate-token-quantitys.ts';
import { generateTokenTree } from './cache-generator/generate-token-tree.ts';
import { compressMaps } from "./cache-generator/compress-maps.ts";
await getDataPerms();
await getCachePerms();
await clearCache();

async function generatePaths(root: string): Promise<[Array<string>, Spinner]>{
    const spinner = Spinner.getInstance();
    const files: Array<string> = []
    await spinner.start(`counting files: ${files.length}`);
    for await (const entry of walk(root)) {
        if(!entry.isFile){
            continue;
        }
        await spinner.setText(`counting files: ${files.length}`);
        files.push(entry.path);
    }
    await spinner.setText(`Found ${files.length} files, writing array to a MessagePack.`);
    return [files, spinner];
}

async function* fileWalker(files: Array<string>): AsyncIterableIterator<[number, string, string]> {
    const spinner = Spinner.getInstance();
    await spinner.start(`Processing file 0/${files.length}`);
    let skipped = 0
    for(const fid in files){
        await spinner.setText(`Processing file ${parseInt(fid)+1}/${files.length} [${files[fid]}]${skipped ? ` (${skipped} skipped)` : ""}`)
        try{
            const path = files[fid];
            const f = await Deno.readTextFile(path);
            yield [parseInt(fid), path, `${path}\r\n${f}`];
        } catch {
            console.error(`Failed to open file ${files[fid]}; Skipping...`)
            skipped++;
        }
    }
    spinner.succeed(`Processed ${files.length}/${files.length} files${skipped ? ` (${skipped} skipped)` : ""}`)
    return;
}

const path_idTree = "./final-cache/id-tree.msgpack"
const path_quantMap = "./temp-cache/quant-map.msgpack"
const path_quantMap_comp = "./temp-cache/quant-map-comp.msgpack"
const path_idQuantMap = "./temp-cache/id-quant-map.msgpack"
const path_idQuantMap_comp = "./temp-cache/id-quant-map.msgpack"
const path_fileMap = "./final-cache/file-map.msgpack"
const path_tokenMap = "./final-cache/token-map.msgpack"
const path_tokenTree = "./final-cache/token-tree.msgpack"

async function Main() {
    // Generate Paths for files.
    const [paths, pathSpinner] = await generatePaths("./test-data");
    Deno.writeFile(path_idTree, safeEncode(paths));
    await pathSpinner.succeed(`${paths.length} files, written to MessagePack '${path_idTree}'.`)
    console.log();

    // Work all the files for words.
    const [quantMap, idQuantMap, fileMap] = await generateTokenCache(fileWalker(paths));
    console.log();

    // Save the data for now.
    const saveQuantSpinner = Spinner.getInstance();
    await saveQuantSpinner.start(`Saving quantMap to '${path_quantMap}'...`);
    Deno.writeFile(path_quantMap, safeEncode(quantMap));
    await saveQuantSpinner.setText(`Saving idQuantMap to '${path_idQuantMap}'...`);
    Deno.writeFile(path_idQuantMap, safeEncode(idQuantMap));
    await saveQuantSpinner.setText(`Saving fileMap to '${path_fileMap}'...`);
    Deno.writeFile(path_fileMap, safeEncode(fileMap));
    await saveQuantSpinner.succeed(`Uncompressed Maps: QuantMap saved to '${path_quantMap}' IdQuantMap saved to '${path_idQuantMap}' fileMap saved to '${path_fileMap}'`);
    console.log()

    // Compress the maps
    const [ compQuantMap, compIdQuantMap, tokenMap ] = compressMaps(quantMap, idQuantMap);

    // Save the data for now.
    const saveCompSpinner = Spinner.getInstance();
    await saveCompSpinner.start(`Saving quantMap to '${path_quantMap_comp}'...`);
    Deno.writeFile(path_quantMap_comp, safeEncode(compQuantMap));
    await saveCompSpinner.setText(`Saving idQuantMap to '${path_idQuantMap_comp}'...`);
    Deno.writeFile(path_idQuantMap_comp, safeEncode(compIdQuantMap));
    await saveCompSpinner.setText(`Saving fileMap to '${path_tokenMap}'...`);
    Deno.writeFile(path_tokenMap, safeEncode(tokenMap));
    await saveCompSpinner.succeed(`Compressed Maps: QuantMap saved to '${path_quantMap_comp}' IdQuantMap saved to '${path_idQuantMap_comp}' tokenMap saved to '${path_tokenMap}'`);
    console.log();

    // Generate TokenTree.
    const tokenTree = await generateTokenTree(compQuantMap, tokenMap, Spinner.getInstance());
    const saveTTSpinner = Spinner.getInstance();
    await saveTTSpinner.start(`Saving Token Tree to ${path_tokenTree}`)
    Deno.writeFile(path_tokenTree, safeEncode(tokenTree));
    await saveTTSpinner.succeed(`Saved Token Tree to ${path_tokenTree}`)
    console.log()
}

await Main()
console.log("All Done!")