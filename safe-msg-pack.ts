import { encode, decode, ExtensionCodec } from "https://esm.sh/@msgpack/msgpack@2.8.0";

const extensionCodec = new ExtensionCodec();

// Set<T>
const SET_EXT_TYPE = 0 // Any in 0-127
extensionCodec.register({
  type: SET_EXT_TYPE,
  encode: (object: unknown): Uint8Array | null => {
    if (object instanceof Set) {
      return safeEncode([...object]);
    } else {
      return null;
    }
  },
  decode: (data: Uint8Array) => {
    const array = safeDecode(data) as Array<unknown>;
    return new Set(array);
  },
});

// Map<T>
const MAP_EXT_TYPE = 1; // Any in 0-127
extensionCodec.register({
  type: MAP_EXT_TYPE,
  encode: (object: unknown): Uint8Array | null => {
    if (object instanceof Map) {
      return safeEncode([...object]);
    } else {
      return null;
    }
  },
  decode: (data: Uint8Array) => {
    const array = safeDecode(data) as Array<[unknown, unknown]>;
    return new Map(array);
  },
});

export function safeEncode(data: unknown){
    return encode(data, { extensionCodec });
}

export function safeDecode(data: Uint8Array){
    return decode(data, { extensionCodec });
}