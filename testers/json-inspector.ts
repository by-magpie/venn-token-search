import Spinner from 'https://deno.land/x/cli_spinners@v0.0.2/mod.ts';
import { Command } from "https://deno.land/x/cliffy@v0.25.7/command/mod.ts";
import { Input, Confirm } from "https://deno.land/x/cliffy@v0.25.7/prompt/mod.ts";

await new Command()
  .name("json-inspector-repl")
  .version("0.1.0")
  .description("A repl for inspecting JSON trees so we don't go mad.")
  .arguments("<input:file>")
  .action((Options, ...Args) => Main(Options, Args))
  .parse(Deno.args);


Deno.addSignalListener("SIGINT", () => {
    console.log();
    Deno.exit();
});

async function Main(__options: unknown, [file, ...__args]: [string, ...unknown[]]) {
  
    const spinner = Spinner.getInstance();
    await spinner.start(`Loading file '${file}' into RAM`);
    let f: string;
    try{
        f = await Deno.readTextFile(file);
    } catch {
        await spinner.fail(`Error opening ${file}, does it exsist?`);
        Deno.exit(1);
    }
    await spinner.setText(`parsing file ${file}`);
    let jsonRoot: unknown;
    try{
        jsonRoot = JSON.parse(f);
    } catch {
        await spinner.fail(`Error Parsing JSON in file ${file}`);
        Deno.exit(1);
    }
    await spinner.succeed(`Ready to query ${file}`);
    
    const path: string[] = [];
    const [__layer, canStillDig, kind] = dig(jsonRoot, path);
    let canDig = canStillDig;
    let layerKind = kind;
    while(true){
        let query: string;
        if(canDig){
            query = await Input.prompt(`/${path.join("/")} <${layerKind}> `);
        } else {
            query = await Input.prompt(`/${path.join("/")} [${layerKind}] `);
        }
        if(query == "\\.."){
            if(path.length == 0){
                const verify: boolean = await Confirm.prompt("Do you want to quit?");
                if(verify){
                    Deno.exit()
                }
                continue;
            }
            path.pop();
        }
        const [layer, canStillDig, kind] = dig(jsonRoot, path);
        canDig = canStillDig;
        layerKind = kind;
        if(query == ""){
            list(layer);
            continue;
        }
        if(!canDig){
            console.log("Can't dig deeper!");
            continue;
        }
        try{
            path.push(query);
            const [layer, canStillDig, kind] = dig(jsonRoot, path);
            if(typeof layer == "undefined"){
                path.pop();
                console.log("Invalid dig!");
                continue;
            }
            canDig = canStillDig;
            layerKind = kind;
        } catch(e) {
            path.pop();
            console.log("Invalid dig!");
            console.log(e)
        }
    }
}

type KINDS = "Object/Record" | "Array" | "Data";

function dig(uklayer: unknown, lastRemaining: string[]): [unknown, boolean, KINDS]{
    const remaining = [...lastRemaining]
    if(typeof uklayer == "object"){

        if(Array.isArray(uklayer)){
            const layer = uklayer as Array<unknown>;
            const nxtRemaining = [...remaining];
            const nxtLayer = nxtRemaining.shift();
            if(nxtLayer == undefined){
                return [layer, true, "Array"];
            } else {
                return dig(layer[parseInt(nxtLayer)], nxtRemaining);
            }
        }

        const layer = uklayer as Record<string, unknown>;
        const nxtRemaining = [...remaining];
        const nxtLayer = nxtRemaining.shift();
        if(nxtLayer == undefined){
            return [layer, true, "Object/Record"];
        } else {
            return dig(layer[nxtLayer], nxtRemaining);
        }
    }
    return [uklayer, false, "Data"];
}

function list(uklayer: unknown){
    if(typeof uklayer == "object"){

        if(Array.isArray(uklayer)){
            const layer = uklayer as Array<unknown>;
            if(layer.length == 0){
                console.log(`Empty Array`)
            }
            console.log(`Array indexes range from 0-${layer.length-1}`)
            return;
        }

        const layer = uklayer as Record<string, unknown>;
        const keys = Object.keys(layer);
        if(keys.length < 25){
            for(const key of keys){
                console.log(key);
            }
        } else {
            let c = 0;
            for(const key of keys){
                console.log(key);
                c++
                if(c >= 24){
                    break;
                }
            }
            console.log(`And ${keys.length-24} more items...`);
        }

        return;
    }
    console.log(`${typeof uklayer}: ${uklayer}`);
}