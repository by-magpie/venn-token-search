import Spinner from 'https://deno.land/x/cli_spinners@v0.0.2/mod.ts';
import { Buffer } from "https://deno.land/std@0.181.0/io/mod.ts";
import { gunzip } from "https://deno.land/x/compress@v0.4.4/mod.ts";
import { emptyDir } from "https://deno.land/std@0.181.0/fs/mod.ts";
import { tar } from "https://deno.land/x/compress@v0.4.4/mod.ts";
const spinner = Spinner.getInstance();


await spinner.start("Downloading all occult files from textfiles.com");
const gzip_buffer = new Buffer(await (await fetch("http://archives.textfiles.com/occult.tar.gz")).arrayBuffer()).bytes();

await spinner.setText("Deflating & saving to disk.");
const tar_buffer = gunzip(gzip_buffer);
await Deno.writeFile("./test-files.tar", tar_buffer);

await spinner.setText("Un-Taring");
await emptyDir("./test-data/");
await tar.uncompress("./test-files.tar", "./test-data/");

await spinner.setText("Cleaning up");
await Deno.remove("./test-files.tar");

await spinner.succeed("test-files downloaded, test-data populated!");