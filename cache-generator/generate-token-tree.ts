import type Spinner from 'https://deno.land/x/cli_spinners@v0.0.2/mod.ts';
import type { tokenQuantityMapCompressed, tokenLayer, tokenIdMap } from "../types.d.ts";

export async function generateTokenTree(
    initialMap: tokenQuantityMapCompressed,
    tokenMap: tokenIdMap,
    spinner?: Spinner
    ) {
    const tokens = [...initialMap.keys()];
    const treeRoot: tokenLayer = new Map();
    const t = tokens.length;
    let c = 0;
    await spinner?.start(`Loading file "./cache/token-array.json" into RAM`);
    for(const [id, quant] of initialMap){
        c++;
        const token = tokenMap.get(id);
        if(!token){
            console.warn(`Missing token ID ${id}! \n\n`);
            continue;
        }
        await spinner?.setText(`Processing token ${c}/${t} [${token}]`)
        digTree(treeRoot, token, id, quant, tokenMap);
    }
    await spinner?.succeed(`${c}/${t} tokens processed!`)
    return treeRoot;
}

function digTree(tree: tokenLayer, search: string, sid: number, quant: number, tokenMap: tokenIdMap): void{
    for(const [id, value] of tree){
        const key = tokenMap.get(id);
        if(!key){
            console.warn(`Missing token ID ${id}! \n\n`);
            continue;
        }
        if(key.includes(search)){
            const [num1, pull] = value;
            tree.delete(id);
            const layer: tokenLayer = new Map();
            layer.set(id, [num1, pull as tokenLayer]);
            tree.set(sid, [quant, layer]);
            return;
        }
        if(search.includes(key)){
            const [__num, layer] = value;
            digTree(layer as tokenLayer, search, sid, quant, tokenMap);
            return;
        }
    }
    tree.set(sid, [quant, new Map()]);
    return;
}