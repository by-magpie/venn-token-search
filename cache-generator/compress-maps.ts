import type {
    tokenQuantityMap,
    tokenQuantityMapCompressed,
    tokenFileMap,
    tokenFileMapCompressed,
    tokenIdMap
} from "../types.d.ts";
export function compressMaps(quantMap: tokenQuantityMap, fileMap: tokenFileMap): [tokenQuantityMapCompressed, tokenFileMapCompressed, tokenIdMap] {
    const compQuantMap: tokenQuantityMapCompressed = new Map();
    const compFileMap: tokenFileMapCompressed = new Map();
    const tokenMap: tokenIdMap = new Map();
    let i = 0
    for(const [key, quant] of quantMap){
        tokenMap.set(i, key);
        compQuantMap.set(i, quant)
        const miniMap = fileMap.get(key);
        if(miniMap){
            compFileMap.set(i, miniMap);
        }
        i++;
    }

    return [compQuantMap, compFileMap, tokenMap]
}