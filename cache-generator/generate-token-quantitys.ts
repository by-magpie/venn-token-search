import type { tokenQuantityMap, tokenFileMap, fileIdMap } from "../types.d.ts";
export async function generateTokenCache(fileItirator: AsyncIterableIterator<[number, string, string]>): Promise<[tokenQuantityMap, tokenFileMap, fileIdMap]> {
    const quantityMap: tokenQuantityMap = new Map();
    const fileIds: fileIdMap = new Map();
    const fileMap: tokenFileMap = new Map();

    // Iterate over all the files
    for await (const [id, key, contents] of fileItirator){
        fileIds.set(id, key);

        // Get the "Words"
        const wordsOnly = /\b((([A-Z]|[a-z]|[0-9])*?)(([A-Z]|[a-z]|[0-9])+)(([A-Z]|[a-z]|[0-9])*?))\b/g;
        const wordArray = contents.matchAll(wordsOnly);

        // Make a local Map
        const wordMap: Map<string, number> = new Map();
        for(const match of wordArray){
            // Only store words in lowercase for sanity
            const word = match[0].toLowerCase();
            wordMap.set(word, (wordMap.get(word) || 0) + 1);
        }

        // Merge the main and local maps
        for(const [word, quant] of wordMap){

            // Add id counts to id map
            const miniFileMap: Map<number, number> = fileMap.get(word) || new Map();
            miniFileMap.set(id, quant);
            fileMap.set(word, miniFileMap);

            // Add counts to count map
            quantityMap.set(word, (quantityMap.get(word) || 0) + quant);
        }
    }
    return [quantityMap, fileMap, fileIds];
}