import { ensureDir } from "https://deno.land/std@0.181.0/fs/mod.ts";
import { emptyDir } from "https://deno.land/std@0.181.0/fs/mod.ts";
export async function getCachePerms(write = true) {
    await getTempCachePerms(write);
    await getFinalCachePerms(write);
}
async function getTempCachePerms(write: boolean) {
    const a = await Deno.permissions.request({ name: "read", path: "./temp-cache" });
    ensureDir("./temp-cache")
    if(a.state == "denied"){
        console.error("Didn't get premission to read cache!")
        Deno.exit(1)
    }
    if(write){
        const a = await Deno.permissions.request({ name: "write", path: "./temp-cache" });
        if(a.state == "denied"){
            console.error("Didn't get premission to read cache!")
            Deno.exit(1)
        }
    }
}
async function getFinalCachePerms(write: boolean) {
    ensureDir("./final-cache")
    const a = await Deno.permissions.request({ name: "read", path: "./final-cache" });
    if(a.state == "denied"){
        console.error("Didn't get premission to read cache!")
        Deno.exit(1)
    }
    if(write){
        const a = await Deno.permissions.request({ name: "write", path: "./final-cache" });
        if(a.state == "denied"){
            console.error("Didn't get premission to read cache!")
            Deno.exit(1)
        }
    }
}

export async function getDataPerms(write = false) {
    const a = await Deno.permissions.request({ name: "read", path: "./test-data" });
    if(a.state == "denied"){
        console.error("Didn't get premission to read cache!")
        Deno.exit(1)
    }
    if(write){
        const a = await Deno.permissions.request({ name: "write", path: "./test-data" });
        if(a.state == "denied"){
            console.error("Didn't get premission to read cache!")
            Deno.exit(1)
        }
    }
}

// Technicilly not a permission, but it goes here anyway.
export async function clearCache() {
    await emptyDir("./temp-cache/")
    await emptyDir("./final-cache/")
    console.log("Cache Cleared.");
}