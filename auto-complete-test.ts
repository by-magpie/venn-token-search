import { iterateReader } from "https://deno.land/std@0.181.0/streams/iterate_reader.ts";
import { safeDecode } from "./safe-msg-pack.ts";
import { getCachePerms } from './test-premissions.ts';
await getCachePerms(false);
import type { tokenLayer, pureCount, tokenIdMap } from './types.d.ts';
    
const treeRootRaw = await Deno.readFile("./final-cache/token-tree.msgpack");
const treeRoot = safeDecode(treeRootRaw) as tokenLayer;
    
const tokenMapRaw = await Deno.readFile("./final-cache/token-map.msgpack");
const tokenMap = safeDecode(tokenMapRaw) as tokenIdMap;

function digToSurface(layer: tokenLayer, search: string): [pureCount, tokenLayer] {
    for(const [id, value] of layer){
        const key = tokenMap.get(id);
        if(!key){
            console.warn(`Missing token ID ${id}! \n\n`);
            continue;
        }
        if(key == search){
            return [value[0], value[1] as tokenLayer]
        }
        if(search.includes(key)){
            return digToSurface(value[1] as tokenLayer, search);
        }
    }
    return [0, layer];
}

function topNAndCanonical(
    n: number,
    sorted: Array<[string, pureCount]>,
    canonacal: [string, pureCount],
    forceAddCannon = true, countCannon = false
    ): Array<[string, pureCount, boolean, boolean]> {
        let count = 0;
        let i = 0;
        const outA: Array<[string, pureCount, boolean, boolean]> = []
        let hasCannon = false;
        while(count < n && i < sorted.length){
            outA.push([...sorted[i], true, false]);
            if(sorted[i][0] == canonacal[0]){
                outA[i][3] = true;
                hasCannon = true;
                if(!countCannon){
                    i++
                    continue
                }
            }
            count++;
            i++;
        }
        if(!hasCannon && forceAddCannon){
            outA.push([...canonacal, false, true]);
        }
        return outA;
}

function getSortedAutoComplete(layer: tokenLayer, search: string, canonacal: number): [Array<[string, pureCount]>] {
    const [usa] = findCompatable(layer, search);
    usa.push([search, canonacal])
    const sa = usa.sort((a, b) => b[1] - a[1])
    return [sa];
}

function findCompatable(layer: tokenLayer, search: string): [Array<[string, pureCount]>] {
    const a: Array<[string, pureCount]> = [];
    for(const [id, value] of layer){
        const key = tokenMap.get(id);
        if(!key){
            console.warn(`Missing token ID ${id}! \n\n`);
            continue;
        }
        if(key.includes(search)){
            const [b] = flattenCompatable(value[1] as tokenLayer);
            a.push([key, value[0]]);
            a.push(...b);
        }
    }
    return [a]
}

function flattenCompatable(layer: tokenLayer): [Array<[string, pureCount]>] {
    const a: Array<[string, pureCount]> = [];
    for(const [id, value] of layer){
        const key = tokenMap.get(id);
        if(!key){
            console.warn(`Missing token ID ${id}! \n\n`);
            continue;
        }
        a.push([key, value[0]]);
        const [b] = flattenCompatable(value[1] as tokenLayer);
        a.push(...b);
    }
    return [a];
}

async function Main() {

    const ESC = '\x1b';
    const CSI = ESC + '['
    const str: string[] = []
    let clearLen = 0;
    Deno.stdin.setRaw(true)
    
    for await (const chunk of iterateReader(Deno.stdin)){
        const charnum = chunk[0]
        const char = new TextDecoder().decode(chunk);
    
        //Exit Codes
        // Esc              CTRL + C?
        if(charnum == 27 || charnum == 397){
            Deno.stdin.setRaw(false)
            console.log(new Array(clearLen - 1).fill("\n").join(""))
            Deno.exit();
        }
    
        // Backspace Code
        if(charnum == 127){
            str.pop();
        }
        // Add Char to string
        else {
            str.push(char);
        }
    
        // Clear Previous Lines
        // const clear = clearLen - 1;
        // let fillLine;
        // if(clear > 0){
        //     fillLine = new Array(clear).fill(`${CSI + 'A'}${CSI + 'K'}`).join("");
        // }
        // await Deno.stdout.write(new TextEncoder().encode(`${CSI + '0G'}${fillLine}`));
    
        // Run Search
        const search = str.join("");
        const [canonacal, surface] = digToSurface(treeRoot, search);
    
        // Sort Search
        const [sortedTop] = getSortedAutoComplete(surface, search, canonacal);
    
        // Get top 5 Items
        const topN = topNAndCanonical(5, sortedTop, [search, canonacal]);
    
        // Generate Results Display
        const lines: string[] = [];
        lines.push((CSI + "G") + (CSI + "J") + `${search}  (${0} results)`);
        for(const top of topN){
            if(top[2])
                lines.push(`${top[0]}: ${top[1]}/${top[2]} ${top[3] ? " <<" : ""}`)
        }
    
        // Fill out Extra Lines
        while(lines.length < clearLen){
            lines.push("");
        }
        await Deno.stdout.write(new TextEncoder().encode(`${lines.join("\n")}`));
        // Save Clear Length
        clearLen = lines.length
        
        // Return to text field
        await Deno.stdout.write(new TextEncoder().encode(CSI + (clearLen - 1) + "A" + CSI + (search.length + 1) + "G"))
    }
}
let err: unknown;
try{
    await Main();
} catch(e) {
    err = e
} finally {
    Deno.stdin.setRaw(false);
    console.log();
}

if(err){
    throw err;
}